import { User } from "next-auth";
import { getUserById, updateUser } from "./db-utils";
import {
  createContact,
  createOrUpdateContactWithReferrere,
} from "./hubspot-utils";

interface UserExtraData {
  hubspotId?: string | null;
  referrer?: string;
}
export const getEnrichedUser = (user: User, userExtraData: UserExtraData) => ({
  ...user,
  role: "guest",
  acceptedTerms: true,
  name: user.name || user.email?.match(/^.+(?=@)/)![0] || "Anonymous Sister",
  ...userExtraData,
});

export const createUnverifiedUser = async (
  email: string,
  referrer?: string
) => {
  const hubspotId = await createContact(email);
  const user = await updateUser(
    getEnrichedUser({ email }, { hubspotId, referrer })
  );
  if (referrer) {
    getUserById(referrer)
      .then(
        (referringUser) =>
          createOrUpdateContactWithReferrere(email, referringUser?.email || "")
        //TODO : add logger
      )
      .catch((e) => console.log(e));
  }
  return user?.id;
};
