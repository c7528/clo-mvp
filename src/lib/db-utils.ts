import { View } from "@src/models/view";
import { connect } from "mongoose";
import { IUser, IUserDocument, User } from "../models/user";

export const connectToDatabase = () => connect(process.env.MONGODB_URI || "");
export const updateUser = async (user: IUser) => {
  try {
    const filter = { email: user.email };
    let doc = await User.findOneAndUpdate(filter, user, {
      upsert: true,
      new: true,
    });
    return doc;
  } catch {}
};

export const setUserRole = async (id: string, role: string) => {
  const filter = { _id: id };
  const update = { role };

  let doc = await User.findOneAndUpdate(filter, update, {
    upsert: true,
    new: true,
  });
  return doc;
};

export const getUser = async (email: string) => {
  const user = await User.find({ email });

  return user[0];
};

export const getUserById = async (_id: string) => {
  const user = await User.find({ _id });
  return user[0];
};

export const getUsers = async () => {
  const users = await User.find({});

  return users;
};

export const getViews = async (slug: string) => {
  const view = await View.findOne({ slug });
  return view;
};

export const updateView = async (slug: string) => {
  const filter = { slug };
  try {
    let doc = await View.findOneAndUpdate(
      filter,
      { $inc: { count: 1 } },
      {
        upsert: true,
        new: true,
      }
    );
    return doc;
  } catch {
    (e: any) => console.log(e);
  }
};
