import { AccessControl } from "accesscontrol";

export const accessControl = new AccessControl();

// admin can manage all users
accessControl
  .grant("admin")
  .resource("users")
  .readAny()
  .createAny()
  .updateAny()
  .deleteAny();

// guests can manage only their own profile
accessControl.grant("guest").resource("users").readOwn().updateOwn();
accessControl.grant("user").resource("users").readAny().updateOwn();

// we lock ACL to avoid updates out of this file
accessControl.lock();
