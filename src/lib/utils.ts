export const fetcher = async (
  url: string,
  method: string = "GET",
  body?: string
) => {
  const res = await fetch(url, {
    body,
    method,
    headers: {
      "Content-Type": "application/json",
    },
  });
  return res.json();
};
