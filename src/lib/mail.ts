import { render } from "mjml-react";
import { createElement, ComponentType } from "react";
import { createTransport, SendMailOptions } from "nodemailer";
import { ThankYouEmail } from "@src/components/email-template";
import { google } from "googleapis";
import SMTPTransport from "nodemailer/lib/smtp-transport";
import Mail from "nodemailer/lib/mailer";

const OAuth2 = google.auth.OAuth2;

const createTransporter = async () => {
  debugger;
  const oauth2Client = new OAuth2(
    process.env.CLIENT_ID,
    process.env.CLIENT_SECRET,
    "https://developers.google.com/oauthplayground"
  );

  oauth2Client.setCredentials({
    refresh_token: process.env.REFRESH_TOKEN,
  });

  const accessToken = await new Promise((resolve, reject) => {
    oauth2Client.getAccessToken((err, token) => {
      if (err) {
        reject();
      }
      resolve(token);
    });
  });

  const transporter = createTransport({
    service: "gmail",
    auth: {
      type: "OAuth2",
      user: process.env.EMAIL,
      accessToken,
      clientId: process.env.CLIENT_ID,
      clientSecret: process.env.CLIENT_SECRET,
      refreshToken: process.env.REFRESH_TOKEN,
    },
  } as SMTPTransport.Options);

  return transporter;
};

// debugger;
const transport = createTransport(process.env.EMAIL_SERVER);

const sendEmail = async (emailOptions: Mail.Options) => {
  let emailTransporter = await createTransporter();
  await emailTransporter.sendMail(emailOptions);
};

export const sendThankYouEmail = async (email: string, id: string) => {
  try {
    await sendEmail({
      to: email,
      from: "noreply@clo.com",
      subject: `Thanks for joining CLO!`,
      text: "text",
      html: createEmailHtml(ThankYouEmail, {
        email,
        referrer: id,
      }),
    });
  } catch (e) {
    console.log(e);
  }
};
export const sendThankYouMail = (email: string, id: string) => {
  return new Promise<void>((resolve, reject) => {
    transport.sendMail(
      {
        to: email,
        from: "noreply@clo.com",
        subject: `Thanks for joining CLO!`,
        text: "text",
        html: createEmailHtml(ThankYouEmail, {
          email,
          referrer: id,
        }),
      },
      (error) => {
        if (error) {
          console.error("SEND_VERIFICATION_EMAIL_ERROR", email, error);
          return reject(error);
        }
        return resolve();
      }
    );
  });
};

export const createEmailHtml = <P extends {}>(
  template: ComponentType<P>,
  props: P
): string => {
  const mjmlElement = createElement(template, props);
  const { html, errors } = render(mjmlElement);
  if (errors?.length) {
    throw new Error(errors[0].message);
  }
  return html;
};
