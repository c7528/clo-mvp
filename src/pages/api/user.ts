import type { NextApiRequest, NextApiResponse } from "next";
import { getUsers } from "@src/lib/db-utils";
import connectDB from "@src/middleware/mongodb";
import { getSession } from "next-auth/client";
import nc from "next-connect";
import { userAuth } from "@src/middleware/user-auth";
import {
  checkAccess,
  getPossessionFromBody,
  getPossessionFromQuery,
} from "@src/middleware/check-access";

const handler = nc()
  .use(userAuth) // injects session into req.session
  .get(
    checkAccess("users", "read:any", getPossessionFromQuery),
    async (req: NextApiRequest, res: NextApiResponse<any>) => {
      try {
        // TODO : add pagination
        // TODO: add agregation to query to select without email
        const users = await getUsers();
        let usersData = [];

        usersData = users.map((user) => ({
          name: user.name,
          image: user.image,
        }));

        res.status(200).json({ users: usersData });
      } catch (err) {
        res.status(500).send("faild to retrieve users");
      }
    }
  );

export default connectDB(handler);
