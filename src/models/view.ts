import mongoose, { Document, model, Model, Schema } from "mongoose";

export interface IView {
  slug: string;
  count: number;
}
export interface IViewDocument extends Document, IView {}

const ViewSchema: Schema = new Schema({
  slug: {
    type: String,
  },
  count: {
    type: Number,
    defualt: 0,
  },
});

export const View: Model<IViewDocument> =
  mongoose.models.View || model("View", ViewSchema);
