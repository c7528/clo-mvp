import Head from "next/head";
import React, { useEffect } from "react";
import { Routing, routes } from "./routing";
import { Switch, Route } from "react-router-dom";
import { Box, Container, Paper, Stack, Theme, Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import Link from "next/link";
import { LightLogo } from "@src/components/light-logo";
import { Logo } from "@src/components/logo";

import { useRouter } from "next/router";
import { fetcher } from "@src/lib/utils";
import { LightMode, DarkMode } from "@mui/icons-material";
import { IconButton } from "@mui/material";
import { ElevationScroll, TopBar } from "@src/components/top-bar";
import {
  UserStateProvider,
  useUserState,
  useCurrentUser,
  useAccess,
} from "@src/context/context";
import { SignInOrOut } from "@src/components/sign-in-or-out";
import { AcceptTerms } from "@src/components/accept";
import { Avatar } from "@src/components/avatar";
import { SkyBackground } from "./sky-background";

const UserSection = () => {
  const user = useCurrentUser();

  return user ? (
    // <Stack direction="row" spacing={2}>
    <>
      <Typography
        variant="h6"
        component="div"
        sx={{ flexGrow: 1 }}
        gutterBottom
      >
        Welcome&nbsp; {user.name}
      </Typography>
      <Avatar user={user} />
    </>
  ) : (
    // </Stack>
    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }} gutterBottom>
      Welcome Guest
    </Typography>
  );
};

// function Users() {
//   const { users } = useUserState();
//   const access = useAccess();

//   return (
//     <>
//       {access("users", "read:any", "any").granted && (
//         <ul style={{ listStyle: "none" }}>
//           {users.map((user, index) => (
//             <li key={index}>
//               <Avatar user={user}></Avatar>
//             </li>
//           ))}
//         </ul>
//       )}
//     </>
//   );
// }

const useStyles = makeStyles((theme: Theme) => ({
  "@global": {
    "*::-webkit-scrollbar": {
      width: "0.4em",
    },
    "*::-webkit-scrollbar-track": {
      "-webkit-box-shadow": "inset 0 0 6px rgba(0,0,0,0.00)",
    },
    "*::-webkit-scrollbar-thumb": {
      backgroundColor: theme.palette.mode === "dark" ? "#38b1eb" : "#6C4DEA",
      outline:
        theme.palette.mode === "dark"
          ? "1px solid #38b1eb"
          : "1px solid #6C4DEA",
      borderRadius: "4px",
    },
    body: {
      margin: 0,
      overflowY: "auto",
      overflowX: "hidden",
      width: "100%",
    },
  },
  root: {
    width: "100wh",
    height: "100vh",
    // position: "relative",
    "& video": {
      objectFit: "cover",
    },

    margin: 0,
  },

  title: {
    paddingBottom: theme.spacing(4),
  },
}));

interface BasicLayoutProps {
  toggleTheme: () => void;
  isDarkTheme: boolean;
}
export const BasicLayout: React.FC<BasicLayoutProps> = ({
  children,
  toggleTheme,
  isDarkTheme,
}) => {
  const router = useRouter();
  const slug = router.pathname;
  const icon = isDarkTheme ? <LightMode /> : <DarkMode />;

  useEffect(() => {
    const registerView = async () => {
      const pageName = slug.length === 1 ? "/home" : slug;
      await fetcher(`/api/views${pageName}`, "POST");
    };

    registerView();
  }, [slug]);

  const classes = useStyles();
  return (
    <UserStateProvider>
      <Head>
        <title>CLO</title>
      </Head>

      <Routing>
        <Paper>
          <SkyBackground isDarkTheme={isDarkTheme}>
            <ElevationScroll>
              <TopBar isDarkTheme={isDarkTheme} toggleTheme={toggleTheme}>
                <UserSection />
                <SignInOrOut />
              </TopBar>
              <Container maxWidth="sm">
                {/* <Image src="/images/logo.gif" height="20px" width="20px"></Image> */}

                <Switch>
                  {routes.map((route, index) => (
                    // Render more <Route>s with the same paths as
                    // above, but different components this time.
                    <Route
                      key={index}
                      path={route.path}
                      exact={route.exact}
                      children={<route.main />}
                    />
                  ))}
                </Switch>
                <AcceptTerms></AcceptTerms>
                {/* {children} */}
              </Container>
            </ElevationScroll>
          </SkyBackground>
        </Paper>
      </Routing>
    </UserStateProvider>
  );
};
