import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

export const routes = [
  {
    path: "/",
    exact: true,
    menu: () => "home",
    main: () => <h2>Homek</h2>,
  },
  {
    path: "/about",
    menu: () => "about",
    main: () => <h2>about</h2>,
  },
  {
    path: "/market",
    menu: () => "market",
    main: () => <h2>Market</h2>,
  },
];
export const Routing: React.FC = ({ children }) => {
  // TODO: add header
  return <Router>{children}</Router>;
};
