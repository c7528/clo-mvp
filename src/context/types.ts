import { IUser } from "@src/models/user";
import { Permission } from "accesscontrol";

export type UsersState = {
  user?: IUser | null;
  loading: boolean;
  access: (
    resource: any,
    action: any,
    possession: any
  ) =>
    | Permission
    | {
        granted: boolean;
      };
};

export const ACCEPT_TERMS = "ACCEPT_TERMS";
export const INIT_STATE = "INIT_STATE";
export const SIGN_IN = "SIGN_IN";
export const SIGN_OUT = "SIGN_OUT";
export const UPDATE_CURRENT_USER = "UPDATE_CURRENT_USER";
export const UPDATE_USERS = "UPDATE_USERS";

// export const ANSWER_QUESTION = "ANSWER_QUESTION";
// export const SUBMIT = "SUBMIT";

export type Actions =
  | { type: "ACCEPT_TERMS"; user: IUser }
  | { type: "INIT_STATE"; state: UsersState }
  | { type: "UPDATE_CURRENT_USER"; user?: IUser | null }
  | { type: "SIGN_IN" }
  | { type: "SIGN_OUT" };
