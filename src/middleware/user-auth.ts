import { getSession } from "next-auth/client";
import { NextApiResponse } from "next";
import { NextHandler } from "next-connect";
import { ExtendedRequest, ExtendedSession } from "@src/types";

// check if user is authenticated
export const userAuth = async (
  req: ExtendedRequest,
  res: NextApiResponse,
  next: NextHandler
) => {
  // store session into request to pass it to following middlewares
  req.session = (await getSession({ req })) as ExtendedSession;
  if (!req.session) {
    return res.status(403).send({
      ok: false,
      message: `User authentication required.`,
    });
  }
  return next();
};
