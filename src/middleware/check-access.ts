import { accessControl } from "@src/lib/access-control";
import user from "@src/pages/api/user";
import { ExtendedRequest } from "@src/types";
import { NextApiResponse } from "next";
import { NextHandler } from "next-connect";

export const getPossessionFromQuery = (req: ExtendedRequest) =>
  req.query.email as string;
export const getPossessionFromBody = (req: ExtendedRequest) =>
  req.body.user.email;

export const checkAccess =
  (
    resource: string,
    action: string,
    getPossession: (req: ExtendedRequest) => string
  ) =>
  (req: ExtendedRequest, res: NextApiResponse<any>, next: NextHandler) => {
    let permission;
    const possession =
      req.session?.user?.email === getPossession(req) ? "own" : "any";
    try {
      permission = accessControl.permission({
        role: req.session?.user?.role,
        resource,
        action,
        possession,
      });
    } catch {
      // `ac.permission` throws if role is not a string
      permission = { granted: false };
    }

    // return 403 if access is denied
    if (!permission.granted) {
      return res.status(403).json({
        ok: false,
        message: "You are not authorized to access this resource",
      });
    }

    return next();
  };
