import { IUser } from "@src/models/user";
import { Avatar as MuiAvatar } from "@mui/material";
export interface IProps {
  user: IUser;
}
export const Avatar: React.FC<IProps> = ({ user }) => {
  const { image, name } = user;
  const stringToColor = (name: string) => {
    let hash = 0;
    let i;

    /* eslint-disable no-bitwise */
    for (i = 0; i < name.length; i += 1) {
      hash = name.charCodeAt(i) + ((hash << 5) - hash);
    }

    let color = "#";

    for (i = 0; i < 3; i += 1) {
      const value = (hash >> (i * 8)) & 0xff;
      color += `00${value.toString(16)}`.substr(-2);
    }
    /* eslint-enable no-bitwise */

    return color;
  };

  const stringAvatar = (name: string) => {
    return {
      sx: {
        bgcolor: stringToColor(name),
      },
      children: `${name.split(" ")[0][0]}${name.split(" ")[1][0] || ""}`,
    };
  };

  if (user.image) {
    return <MuiAvatar alt={user.name} src={user.image} />;
  }
  const stringAvatarData = stringAvatar(name + " ");
  return <MuiAvatar {...stringAvatarData} />;
};
