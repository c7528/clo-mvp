import { Button } from "@mui/material";
import { signOut } from "next-auth/client";
import React from "react";

export const SignOut = () => (
  <Button color="inherit" onClick={() => signOut()}>
    Sign out
  </Button>
);
