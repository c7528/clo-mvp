import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import { Menu as MenuIcon } from "@mui/icons-material";
import useScrollTrigger from "@mui/material/useScrollTrigger";
import PopupMenu from "@src/components/popup-menu";
import Link from "next/link";
import { LightLogo } from "./light-logo";
import { Logo } from "./logo";
import { LightMode, DarkMode } from "@mui/icons-material";

export const ElevationScroll: React.FC = ({ children }) => {
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
    target: window,
  });

  return React.cloneElement(<>{children}</>, {
    elevation: trigger ? 4 : 0,
  });
};
interface TopBarProps {
  isDarkTheme: boolean;
  toggleTheme: () => void;
}
export const TopBar: React.FC<TopBarProps> = ({
  isDarkTheme,
  toggleTheme,
  children,
}) => {
  const icon = isDarkTheme ? <LightMode /> : <DarkMode />;

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" enableColorOnDark>
        <Toolbar>
          <IconButton
            edge="end"
            color="inherit"
            aria-label="mode"
            onClick={toggleTheme}
          >
            {icon}
          </IconButton>
          <Link href="/">
            <a>{isDarkTheme ? <LightLogo /> : <Logo />}</a>
          </Link>
          <PopupMenu />
          {children}
        </Toolbar>
      </AppBar>
    </Box>
  );
};
