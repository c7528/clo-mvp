import React, { FC } from "react";
import {
  Mjml,
  MjmlAll,
  MjmlAttributes,
  MjmlBody,
  MjmlBodyProps,
  MjmlBreakpoint,
  MjmlFont,
  MjmlHead,
  MjmlPreview,
  MjmlTitle,
  MjmlImage,
} from "mjml-react";

export interface EmailBaseProps extends MjmlBodyProps {
  breakpoint?: number;
  title: string;
  preview: string;
}
export const EmailBase: FC<EmailBaseProps> = ({
  breakpoint,
  children,
  title,
  preview,
  width,
  ...restBodyProps
}) => {
  const fontFamily = "Roboto";
  const fontHref = `http://fonts.googleapis.com/css?family=${fontFamily}:400,100,100`;
  return (
    <Mjml>
      <MjmlHead>
        <MjmlTitle>{title}</MjmlTitle>
        <MjmlPreview>{preview}</MjmlPreview>
        <MjmlFont name={fontFamily} href={fontHref} />
        <MjmlAttributes>
          <MjmlAll fontFamily={fontFamily} fontSize={14} color="black" />
        </MjmlAttributes>
        {breakpoint && <MjmlBreakpoint width={breakpoint} />}
      </MjmlHead>
      <MjmlBody {...restBodyProps}>{children}</MjmlBody>
    </Mjml>
  );
};
