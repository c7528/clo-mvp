import { MjmlColumn, MjmlSection, MjmlText, MjmlImage } from "mjml-react";
import React, { FC } from "react";
import { EmailBase } from "./email-base";
export interface HelloEmailProps {
  email: string;
  referrer: string;
}
export const ThankYouEmail: FC<HelloEmailProps> = ({ email, referrer }) => {
  const link = `theclo.co/?r=${referrer}`;

  return (
    <EmailBase title="Thank You" preview="Hello from CLO!">
      <MjmlSection>
        <MjmlColumn>
          <MjmlImage src="https://www.theclo.co/images/logo.png" />
          <MjmlText>Smart move!</MjmlText>
          <MjmlText>
            We&apos;ve added your email to the CLO waiting list.
          </MjmlText>
          <MjmlText>Thank you for joining us</MjmlText>
          <MjmlText>
            The first 5,000 users get priority access to all the new fetures and
            exclusive giveaways.
          </MjmlText>
          <MjmlText>
            Ask your freinds and collegues to join the CLO waitlist using your
            unique link bellow
          </MjmlText>
          <MjmlText>
            {" "}
            <a href={link}>{link}</a>
          </MjmlText>
          <MjmlText>
            You may with­draw your email from the waiting list by emailing us at{" "}
            <a href="mailto:contact@theclo.co?subject=unsbscribe CLO&body=please remove me from waiting list">
              contact@theclo.co
            </a>
          </MjmlText>
        </MjmlColumn>
      </MjmlSection>
    </EmailBase>
  );
};
