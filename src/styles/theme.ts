import { Theme, ThemeOptions } from "@mui/material/styles";
import { createTheme, responsiveFontSizes } from "@mui/material/styles";

export const paletteColorsDark = {
  primary: "#0f4c75",
  secondary: "#3282b8",
  error: "#E44C65",
  background: "#1b262c",
  text: "#bbe1fa",
};

export const paletteColorsLight = {
  primary: "#6886c5",
  secondary: "#ffe0ac",
  error: "#E44C65",
  background: "#f9f9f9",
  text: "#050505",
};

const lightColorPalette: ThemeOptions = {
  palette: {
    mode: "light",
    text: {
      primary: "#012147",
    },
    primary: {
      main: "#6C4DEA",
    },
    secondary: {
      main: "#012147",
    },
    background: {
      default: "transparent",
      paper: "transparent",
    },
  },
};

const darkColorPalette: ThemeOptions = {
  palette: {
    mode: "dark",
    text: {
      primary: "#ffffff",
    },
    primary: {
      main: "#38b1eb",
    },
    secondary: {
      main: "#ffffff",
    },
    background: {
      default: "transparent",
      paper: "transparent",
    },
  },
};

const typography = {
  fontFamily: [
    "Raleway",
    "Roboto",
    '"Helvetica Neue"',
    "Arial",
    "sans-serif",
  ].join(","),
  fontSize: 14,
  fontWeightLight: 300,
  fontWeightRegular: 400,
  fontWeightMedium: 500,
};
export const darkTheme = responsiveFontSizes(
  createTheme({ ...darkColorPalette, typography })
);
export const lightTheme = responsiveFontSizes(
  createTheme({ ...lightColorPalette, typography })
);
