/// <reference types="cypress" />

describe("Take the quiz", () => {
  const token =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkdpbGFkIEJvcm5zdGVpbiIsImlhdCI6MTUxNjIzOTAyMiwidWlkIjoia3VrdSJ9.L4d-ZtMitwJRlWB-Edxzla0m9UgDG925iUzcWE4c9jA";
  beforeEach(() => {
    cy.visit(`http://localhost:3000?token=${token}`);
    cy.get('[data-hook="accept"]').click();
  });

  it("should navigate to the quiz page", () => {
    cy.url().should("include", "/quiz");
  });

  it("should render first question", () => {
    cy.url().should("include", "/quiz");
    cy.get("p").contains("Question 1/60");
  });

  it("should render second question once first question is answered", () => {
    cy.url().should("include", "/quiz");
    cy.get('[data-hook="answer"]').first().click();
    cy.get("p").contains("Question 2/60");
  });

  it("should render second question once first question is answered", () => {
    cy.url().should("include", "/quiz");
    for (let i = 0; i < 60; i++) {
      cy.get('[data-hook="answer"]').first().click();
    }
    cy.url().should("include", "/check");
    cy.get('[data-hook="see-results"]').should("be.visible");
  });
});
