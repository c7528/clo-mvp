/** @type {import('next').NextConfig} */
const withPWA = require("next-pwa");
const runtimeCaching = require("next-pwa/cache");

module.exports = withPWA({
  reactStrictMode: true,
  productionBrowserSourceMaps: true,
  async rewrites() {
    return [
      {
        source: "/:any*",
        destination: "/",
      },
    ];
  },

  pwa: {
    dest: "public",
    // register: true,
    // skipWaiting: true,
    runtimeCaching,
  },
});
